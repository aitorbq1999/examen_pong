/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    // LOGO
    Texture2D logo = LoadTexture("Resources/logo.png");
    float alpha = 0.0f;
    bool fadeIn = true;
    
    // TITLE
    Font pixelFont = LoadFont("Resources/pixel.ttf");
    char titleText[16] = "FINAL PONG!";
    char startText[16] = "press enter";
    int titleSize = 80;
    Vector2 titlePos = { screenWidth/2 - MeasureTextEx(pixelFont, titleText, titleSize, 0).x/2, -100 };
    bool blink = false;
    
    //GAMEPLAY
    Color ballColor = { 102, 183, 104, 255 };
    Color playerColor = { 1, 87, 183, 255 };
    Color playerFillColor = { 83, 144, 212, 255 };
    Color enemyColor = { 164, 1, 183, 255 };
    Color enemyFillColor = { 210, 93, 224, 255 };
    
    float topMargin = 50;
    float bgMargin = 8;
    float fieldMargin = 10;
    float counterMargin = 25;
    float lifeMargin = bgMargin;
    
    Rectangle bgRec = {0, topMargin, screenWidth, screenHeight - topMargin };
    Rectangle fieldRec = { bgRec.x + bgMargin, bgRec.y + bgMargin, bgRec.width - bgMargin*2, bgRec.height - bgMargin*2 };
    
    Rectangle playerBgRec = { 0, 0, screenWidth/2 - counterMargin, topMargin };
    Rectangle playerFillRec = { playerBgRec.x +lifeMargin, playerBgRec.y + lifeMargin, playerBgRec.width - lifeMargin*2, playerBgRec.height - lifeMargin*2};
    Rectangle playerLifeRec = playerFillRec ;
    
    Rectangle enemyBgRec = { screenWidth/2 + counterMargin, 0, screenWidth/2 - counterMargin, topMargin };
    Rectangle enemyFillRec = { enemyBgRec.x +lifeMargin, enemyBgRec.y + lifeMargin, enemyBgRec.width - lifeMargin*2, enemyBgRec.height - lifeMargin*2};
    Rectangle enemyLifeRec = enemyFillRec ;
    
    Rectangle player = { fieldRec.x + fieldMargin, fieldRec.y + fieldRec.height/2 - 40, 20, 80 };
    int playerSpeedY = 4;
    
    Rectangle enemy = { fieldRec.x + fieldRec.width - fieldMargin -20, fieldRec.y + fieldRec.height/2 - 40, 20, 80 };
    int enemySpeedY = 4;
    
    Vector2 ballPosition = { fieldRec.x + fieldRec.width/2, fieldRec.y +fieldRec.height/2 };
    Vector2 ballSpeed = { 0, 0 } ;
    int ballRadius = 20;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if (fadeIn)
                {
                    alpha += 1.0f/90;
                
                    if (alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        
                        framesCounter++;
                        if (framesCounter % 120 == 0)
                            {
                                framesCounter = 0;
                                fadeIn = false;
                            }
                    }
                }
                else
                {
                    alpha -= 1.0f/90;
                    
                    if (alpha <= 0.0f)
                    {
                        //TITLESCREEN
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                
                if(IsKeyPressed(KEY_ENTER))
                {
                    framesCounter = 0;
                    screen = TITLE;
                }
                
                
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (titlePos.y < 100)
                {
                    titlePos.y += 2;
                    
                    if (IsKeyPressed(KEY_ENTER))
                        {
                            titlePos.y = 100;
                        }
                }
                else 
                {
                    titlePos.y = 100;
                    
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                    framesCounter++;
                    if(framesCounter % 8 == 0)
                    {
                        framesCounter = 0;
                        blink = !blink;
                    }
                    
                    if (IsKeyPressed(KEY_ENTER))
                    {
                        framesCounter = 0;
                        screen = GAMEPLAY;
                    }
                }
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                
                // TODO: Player movement logic.......................(0.2p)
                
                // TODO: Enemy movement logic (IA)...................(1p)
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTexture(logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, Fade(WHITE, alpha));
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawTextEx(pixelFont, titleText, titlePos, titleSize, 0, BLACK);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink) DrawText(startText, screenWidth/2 - MeasureText(startText, 20)/2, screenHeight - 100, 20, BLACK);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangleRec(bgRec, BLACK);
                    DrawRectangleRec(fieldRec, LIME);
                    DrawLineEx((Vector2){bgRec.x + bgRec.width/2, bgRec.y}, (Vector2){bgRec.x + bgRec.width/2, bgRec.y + bgRec.height}, bgMargin, BLACK);
                    
                    DrawCircleV(ballPosition, ballRadius, ballColor);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, playerColor);
                    DrawRectangleRec(enemy, enemyColor);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(playerBgRec, BLACK);
                    DrawRectangleRec(playerFillRec, playerColor);
                    DrawRectangleRec(playerLifeRec, playerFillColor);
                    
                    DrawRectangleRec(enemyBgRec, BLACK);
                    DrawRectangleRec(enemyFillRec, enemyColor);
                    DrawRectangleRec(enemyLifeRec, enemyFillColor);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(logo);
    UnloadFont(pixelFont);
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}