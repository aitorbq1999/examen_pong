/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    // LOGO
    Texture2D logo = LoadTexture("Resources/logo.png");
    float alpha = 0.0f;
    bool fadeIn = true;
    
    // TITLE
    Font pixelFont = LoadFont("Resources/pixel.ttf");
    char titleText[16] = "FINAL PONG!";
    char startText[16] = "press enter";
    int titleSize = 80;
    Vector2 titlePos = { screenWidth/2 - MeasureTextEx(pixelFont, titleText, titleSize, 0).x/2, -100 };
    bool blink = false;
    
    //GAMEPLAY
    Color ballColor = { 102, 183, 104, 255 };
    Color playerColor = { 1, 87, 183, 255 };
    Color playerFillColor = { 83, 144, 212, 255 };
    Color enemyColor = { 164, 1, 183, 255 };
    Color enemyFillColor = { 210, 93, 224, 255 };
    
    float topMargin = 50;
    float bgMargin = 8;
    float fieldMargin = 10;
    float counterMargin = 35;
    float lifeMargin = bgMargin;
    
    Rectangle bgRec = {0, topMargin, screenWidth, screenHeight - topMargin };
    Rectangle fieldRec = { bgRec.x + bgMargin, bgRec.y + bgMargin, bgRec.width - bgMargin*2, bgRec.height - bgMargin*2 };
    
    Rectangle playerBgRec = { 0, 0, screenWidth/2 - counterMargin, topMargin };
    Rectangle playerFillRec = { playerBgRec.x +lifeMargin, playerBgRec.y + lifeMargin, playerBgRec.width - lifeMargin*2, playerBgRec.height - lifeMargin*2};
    Rectangle playerLifeRec = playerFillRec ;
    
    Rectangle enemyBgRec = { screenWidth/2 + counterMargin, 0, screenWidth/2 - counterMargin, topMargin };
    Rectangle enemyFillRec = { enemyBgRec.x +lifeMargin, enemyBgRec.y + lifeMargin, enemyBgRec.width - lifeMargin*2, enemyBgRec.height - lifeMargin*2};
    Rectangle enemyLifeRec = enemyFillRec ;
    
    Rectangle player = { fieldRec.x + fieldMargin, fieldRec.y + fieldRec.height/2 - 40, 20, 80 };
    int playerSpeedY = 4;
    
    Rectangle enemy = { fieldRec.x + fieldRec.width - fieldMargin -20, fieldRec.y + fieldRec.height/2 - 40, 20, 80 };
    int enemySpeedY = 3;
    float AImargin = enemy.height/4;
    
    Vector2 ballPosition = { fieldRec.x + fieldRec.width/2, fieldRec.y +fieldRec.height/2 };
    Vector2 ballSpeed = { GetRandomValue( 4, 5), GetRandomValue( 5, 6)} ;
    
    if(GetRandomValue( 0, 1)) ballSpeed.x *= -1;
    if(GetRandomValue( 0, 1)) ballSpeed.y *= -1;
    
    int ballRadius = 20;
    
    int playerLife = 5;
    int enemyLife = 5;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    bool pause = false;
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if (fadeIn)
                {
                    alpha += 1.0f/90;
                
                    if (alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        
                        framesCounter++;
                        if (framesCounter % 120 == 0)
                            {
                                framesCounter = 0;
                                fadeIn = false;
                            }
                    }
                }
                else
                {
                    alpha -= 1.0f/90;
                    
                    if (alpha <= 0.0f)
                    {
                        //TITLESCREEN
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                
                if(IsKeyPressed(KEY_ENTER))
                {
                    framesCounter = 0;
                    screen = TITLE;
                }
                
                
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (titlePos.y < 100)
                {
                    titlePos.y += 2;
                    
                    if (IsKeyPressed(KEY_ENTER))
                        {
                            titlePos.y = 100;
                        }
                }
                else 
                {
                    titlePos.y = 100;
                    
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                    framesCounter++;
                    if(framesCounter % 8 == 0)
                    {
                        framesCounter = 0;
                        blink = !blink;
                    }
                    
                    if (IsKeyPressed(KEY_ENTER))
                    {
                        framesCounter = 0;
                        screen = GAMEPLAY;
                    }
                }
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                ballPosition.x += ballSpeed.x;
                ballPosition.y += ballSpeed.y;
                // TODO: Player movement logic.......................(0.2p)
                if (IsKeyDown(KEY_UP)) player.y -= playerSpeedY;
                if (IsKeyDown(KEY_DOWN)) player.y += playerSpeedY;
                
                if (player.y < fieldRec.y) player.y = fieldRec.y;
                if (player.y > fieldRec.y + fieldRec.height - player.height) player.y = fieldRec.y + fieldRec.height - player.height;
                // TODO: Enemy movement logic (IA)...................(1p)
                if ((ballPosition.x > fieldRec.x + fieldRec.width/2) && ballSpeed.x > 0)
                {
                    if (ballPosition.y < enemy.y + enemy.height/2 - AImargin) enemy.y -= enemySpeedY;
                    if (ballPosition.y > enemy.y + enemy.height/2 + AImargin) enemy.y += enemySpeedY;
                }
                
                if (enemy.y < fieldRec.y) enemy.y = fieldRec.y;
                if (enemy.y > fieldRec.y + fieldRec.height - enemy.height) enemy.y = fieldRec.y + fieldRec.height - enemy.height;
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                 if (CheckCollisionCircleRec(ballPosition, ballRadius, player) && ballSpeed.x < 0)
                 {
                     ballSpeed.x *= -1;
                 }
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy) && ballSpeed.x > 0)
                 {
                     ballSpeed.x *= -1;
                 }
                // TODO: Collision detection (ball-limits) logic.....(1p)
                    //TOP LIMITS
                if ((ballPosition.y - ballRadius < fieldRec.y) && ballSpeed.y < 0)
                {
                    ballSpeed.y *= -1;
                }
                    //BOTTON LIMITS
                if ((ballPosition.y + ballRadius > fieldRec.y + fieldRec.height) && ballSpeed.y > 0)
                {
                    ballSpeed.y *= -1;
                }
                
                     //LEFT LIMITS
                if ((ballPosition.x - ballRadius < fieldRec.x) && ballSpeed.x < 0)
                {
                    ballSpeed.x *= -1;
                }
                    //RIGHT LIMITS
                if ((ballPosition.x + ballRadius > fieldRec.x + fieldRec.width) && ballSpeed.x > 0)
                {
                    ballSpeed.x *= -1;
                }
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)
                
                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTexture(logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, Fade(WHITE, alpha));
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawTextEx(pixelFont, titleText, titlePos, titleSize, 0, BLACK);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink) DrawText(startText, screenWidth/2 - MeasureText(startText, 20)/2, screenHeight - 100, 20, BLACK);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangleRec(bgRec, BLACK);
                    DrawRectangleRec(fieldRec, LIME);
                    DrawLineEx((Vector2){bgRec.x + bgRec.width/2, bgRec.y}, (Vector2){bgRec.x + bgRec.width/2, bgRec.y + bgRec.height}, bgMargin, BLACK);
                    
                    DrawCircleV(ballPosition, ballRadius, ballColor);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, playerColor);
                    DrawRectangleRec(enemy, enemyColor);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(playerBgRec, BLACK);
                    DrawRectangleRec(playerFillRec, playerColor);
                    DrawRectangleRec(playerLifeRec, playerFillColor);
                    
                    DrawRectangleRec(enemyBgRec, BLACK);
                    DrawRectangleRec(enemyFillRec, enemyColor);
                    DrawRectangleRec(enemyLifeRec, enemyFillColor);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - MeasureText(FormatText("%i", secondsCounter), 40)/2, topMargin -42, 40, ballColor);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause)
                    {
                        DrawRectangle( 0, 0, screenWidth, screenHeight, Fade(WHITE, 0.8f));
                        DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE", 30)/2, screenHeight/2 - 15, 30, BLACK); 
                        
                    }
                    
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(logo);
    UnloadFont(pixelFont);
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}